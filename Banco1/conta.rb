class ContaBancaria
    def initialize(proprietario, valor)
        @proprietario = proprietario
        @valor = valor
    end

    def transferir(outra_conta, valor)
        if saldo >= valor
            debitar(valor)
            outra_conta.depositar(valor)
        else
            puts("Saldo insuficiente")
        end

    end

    def saldo
        @valor
    end

    def debitar(valor_debitar)
        @valor -= valor_debitar
    end

    def depositar(valor_depositar)
        @valor += valor_depositar
    end
end

